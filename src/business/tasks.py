from google.cloud import datastore

_kind = 'Task'
_builtin_list = list


def _get_client():

    """ Get the Datastore client """

    return datastore.Client()


def _from_datastore(entity):

    """ Parse the given entity into a dictionary """

    if not entity:
        return entity
    if isinstance(entity, _builtin_list):
        entity = entity.pop()
    entity['id'] = entity.key.id
    return entity


def list(limit=100):

    """ List the tasks collection """

    ds = _get_client()
    query = ds.query(kind=_kind, order=['-creation_date'])
    query_iterator = query.fetch(limit=limit)
    page = next(query_iterator.pages)
    return _builtin_list(map(_from_datastore, page))


def create(data):

    """ Create the given task """

    ds = _get_client()
    key = ds.key(_kind)
    entity = datastore.Entity(
        key=key,
        exclude_from_indexes=['title']
    )
    entity.update(data)
    ds.put(entity)
    return _from_datastore(entity)


def delete(task_id):

    """ Delete the given task """

    ds = _get_client()
    key = ds.key(_kind, int(task_id))
    ds.delete(key)
